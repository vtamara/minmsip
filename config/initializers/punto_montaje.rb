# relative_url_root configurada en config/application.rb
Minmsip::Application.config.assets.prefix = ENV.fetch(
  'RUTA_RELATIVA', '/minmsip') == '/' ?
 '/assets' : (ENV.fetch('RUTA_RELATIVA', '/minmsip') + '/assets')
