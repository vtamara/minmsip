require_relative "boot"

require "rails"                                                                                                                                                 
# Elige los marcos de trabajo que necesitas:
require "active_model/railtie"                                                                                                                                  
require "active_job/railtie"                                                                                                                                    
require "active_record/railtie"                                                                                                                                 
# require "active_storage/engine"                                                                                                                               
require "action_controller/railtie"                                                                                                                             
require "action_mailer/railtie"                                                                                                                                 
# require "action_mailbox/engine"                                                                                                                               
# require "action_text/engine"                                                                                                                                  
require "action_view/railtie"                                                                                                                                   
require "action_cable/engine"                                                                                                                                   
require "rails/test_unit/railtie"

Bundler.require(*Rails.groups)

module Minmsip
  class Application < Rails::Application
    config.load_defaults 7.1

    config.autoload_lib(ignore: %w(assets tasks))

    config.active_record.schema_format = :sql
    config.railties_order = [:main_app, Msip::Engine, :all]

    config.time_zone = 'America/Bogota'
    config.i18n.default_locale = :es

    config.relative_url_root = ENV.fetch('RUTA_RELATIVA', '/minmsip/') 

    config.x.formato_fecha = ENV.fetch('MSIP_FORMATO_FECHA', 'dd/M/yyyy')
    config.hosts.concat(
      ENV.fetch('CONFIG_HOSTS', '127.0.0.1').downcase.split(',')
    )
  end
end
