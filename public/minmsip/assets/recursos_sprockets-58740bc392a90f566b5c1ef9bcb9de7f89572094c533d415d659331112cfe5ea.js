(function($) {

  var cocoon_element_counter = 0;

  var create_new_id = function() {
    return (new Date().getTime() + cocoon_element_counter++);
  }

  var newcontent_braced = function(id) {
    return '[' + id + ']$1';
  }

  var newcontent_underscord = function(id) {
    return '_' + id + '_$1';
  }

  var getInsertionNodeElem = function(insertionNode, insertionTraversal, $this){

    if (!insertionNode){
      return $this.parent();
    }

    if (typeof insertionNode == 'function'){
      if(insertionTraversal){
        console.warn('association-insertion-traversal is ignored, because association-insertion-node is given as a function.')
      }
      return insertionNode($this);
    }

    if(typeof insertionNode == 'string'){
      if (insertionTraversal){
        return $this[insertionTraversal](insertionNode);
      }else{
        return insertionNode == "this" ? $this : $(insertionNode);
      }
    }

  }

  $(document).on('click', '.add_fields', function(e) {
    e.preventDefault();
    var $this                 = $(this),
        assoc                 = $this.data('association'),
	assocs                = $this.data('associations'),
        content               = $this.data('association-insertion-template'),
        count                 = parseInt($this.data('count'), 10),
        regexp_braced         = new RegExp('\\[new_' + assoc + '\\](.*?\\s)', 'g'),
        regexp_underscord     = new RegExp('_new_' + assoc + '_(\\w*)', 'g'),
        new_id                = null,
        new_content           = null;

    count = (isNaN(count) ? 1 : Math.max(count, 1));

    if (count == 1 && $this.data('ajax') && $this.data('ajaxdata')) {
      var cid                 = $this.data("ajaxdata"),
          mdata               = {},
          regexp_inputid      = new RegExp('<input .*id="[^"]*_' + assoc + 
			  '_id"', 'g'),
          regexp_inputid2     = new RegExp('<input .*id="[^"]*_' + assocs + 
			  '_id"', 'g');
      mdata[cid] = $('#' + cid).val();
      if (mdata[cid] == undefined) {
        wrapper_class = $this.data('wrapper-class') || 'nested-fields';
      	mdata[cid] =  $(this).closest('.' + wrapper_class).
		find('.' + cid + ' input').val();
      }
	      

      $.ajax($this.data("ajax"), {
        type: 'GET',
        dataType: 'json', 
        data: mdata
      }).done(function(pnew_id) { 
        if (typeof pnew_id == "string" || typeof pnew_id == "number") {
          new_content = content.replace(regexp_inputid, 
            '$& value="' + pnew_id + '" ');
          new_content = new_content.replace(regexp_inputid2, 
            '$& value="' + pnew_id + '" ');
	  new_id = pnew_id;
        } else {
          if (!(assoc in pnew_id)) {
            alert( "Cocoon request failed, json returned should include key " 
		    + assoc + " with identification of new association");
          }
          new_id = pnew_id[assoc]
          new_content = content.replace(regexp_inputid, 
            '$& value="' + new_id + '" ');
          new_content = new_content.replace(regexp_inputid2, 
            '$& value="' + new_id + '" ');
          for (var i in pnew_id) {
            if (i != assoc) {
              // We tried by converting to jquery and using val and html but
              // didn't change the generated html
              var regexp_secinputid = new RegExp(
                  '<input .*id="[^"]*_' + i + '_attributes_id"', 'g');
              new_content = new_content.replace(regexp_secinputid, 
                  '$& value="' + pnew_id[i] + '" ');
              var regexp_input = new RegExp(
	          '<input .*id="[^"]*_new_' + assoc + '_' + i + '"', 'g');
              new_content = new_content.replace(regexp_input, 
                  '$& value="' + pnew_id[i] + '" ');
              var regexp_input2 = new RegExp(
	          '<input .*id="[^"]*_new_' + assocs + '_' + i + '"', 'g');
              new_content = new_content.replace(regexp_input2, 
                  '$& value="' + pnew_id[i] + '" ');
              var regexp_select = new RegExp(
	          '<select .*id="[^"]*_new_' + assoc + '_' + i + 
                  '".* <option value="' + pnew_id[i] + '"', 'g');
              new_content = new_content.replace(regexp_input, 
                  '$& selected');
            } 
          }
        }
        new_content2 = new_content.replace(regexp_braced, newcontent_braced(new_id));
        if (new_content2 != new_content) {
          new_content2 = new_content2.replace(regexp_underscord,
              newcontent_underscord(new_id));
        }
	add_fields($this, assoc, assocs, new_content2, count, regexp_braced,
			new_id, new_content2);
      }).fail(function(jqXHR, textStatus) {
        alert( "Cocoon request failed: " + textStatus );
      });
    } else {
      new_id = create_new_id();
      new_content = content.replace(regexp_braced, newcontent_braced(new_id));
      add_fields($this, assoc, assocs, content, count, regexp_braced, 
		      new_id, new_content);
    }
  });


  /* Complete event click on .add_fields once we know new_id */
  function add_fields($this, assoc, assocs, content, count, regexp_braced, 
		  new_id, new_content)  {
    var insertionMethod       = $this.data('association-insertion-method') || $this.data('association-insertion-position') || 'before',
        insertionNode         = $this.data('association-insertion-node'),
        insertionTraversal    = $this.data('association-insertion-traversal'),
        regexp_underscord     = new RegExp('_new_' + assoc + '_(\\w*)', 'g'),
        new_contents          = [];

    if (new_content == content) {
      regexp_braced     = new RegExp('\\[new_' + assocs + '\\](.*?\\s)', 'g');
      regexp_underscord = new RegExp('_new_' + assocs + '_(\\w*)', 'g');
      new_content       = content.replace(regexp_braced, newcontent_braced(new_id));
    }

    new_content = new_content.replace(regexp_underscord, newcontent_underscord(new_id));
    new_contents = [new_content];

    count -= 1;

    while (count) {
      new_id      = create_new_id();
      new_content = content.replace(regexp_braced, newcontent_braced(new_id));
      new_content = new_content.replace(regexp_underscord, newcontent_underscord(new_id));
      new_contents.push(new_content);

      count -= 1;
    }

    var insertionNodeElem = getInsertionNodeElem(insertionNode, insertionTraversal, $this)

    if( !insertionNodeElem || (insertionNodeElem.length == 0) ){
      console.warn("Couldn't find the element to insert the template. Make sure your `data-association-insertion-*` on `link_to_add_association` is correct.")
    }

    $.each(new_contents, function(i, node) {
      var contentNode = $(node);

      var before_insert = jQuery.Event('cocoon:before-insert');
      insertionNodeElem.trigger(before_insert, [contentNode]);

      if (!before_insert.isDefaultPrevented()) {
        // allow any of the jquery dom manipulation methods (after, before, append, prepend, etc)
        // to be called on the node.  allows the insertion node to be the parent of the inserted
        // code and doesn't force it to be a sibling like after/before does. default: 'before'
        var addedContent = insertionNodeElem[insertionMethod](contentNode);

        insertionNodeElem.trigger('cocoon:after-insert', [contentNode]);
      }
    });
  }


  $(document).on('click', '.remove_fields.dynamic, .remove_fields.existing', function(e) {
    var $this = $(this),
        wrapper_class = $this.data('wrapper-class') || 'nested-fields',
        node_to_delete = $this.closest('.' + wrapper_class),
        trigger_node = node_to_delete.parent();

    e.preventDefault();

    var before_remove = jQuery.Event('cocoon:before-remove');
    trigger_node.trigger(before_remove, [node_to_delete]);

    if (!before_remove.isDefaultPrevented()) {
      var timeout = trigger_node.data('remove-timeout') || 0;

      setTimeout(function() {
        if ($this.hasClass('dynamic')) {
            node_to_delete.detach();
        } else {
            $this.prev("input[type=hidden]").val("1");
            node_to_delete.hide();
        }
        trigger_node.trigger('cocoon:after-remove', [node_to_delete]);
      }, timeout);
    }
  });


  $(document).on("ready page:load turbolinks:load", function() {
    $('.remove_fields.existing.destroyed').each(function(i, obj) {
      var $this = $(this),
          wrapper_class = $this.data('wrapper-class') || 'nested-fields';

      $this.closest('.' + wrapper_class).hide();
    });
  });

})(jQuery);


(function() {
  this.busca_campo_similar = function(idactual, tipoactual, tipobuscado) {
    var idb;
    idb = idactual.replace('id_' + tipoactual, 'id_' + tipobuscado);
    if (idb !== idactual && $('#' + idb).length > 0) {
      return idb;
    }
    idb = idactual.replace(tipoactual + '_id', tipobuscado + '_id');
    if (idb !== idactual && $('#' + idb).length > 0) {
      return idb;
    }
    idb = idactual.replace('_' + tipoactual, '_' + tipobuscado);
    if (idb !== idactual && $('#' + idb).length > 0) {
      return idb;
    }
    return "";
  };

  this.pone_coord = function(root, tabla, id, nomcampo) {
    var idlat, idlon, lat, lon, modelo, y;
    switch (tabla) {
      case 'pais':
        modelo = 'paises';
        break;
      case 'departamento':
        modelo = 'departamentos';
        break;
      case 'municipio':
        modelo = 'municipios';
        break;
      case 'clase':
        modelo = 'clases';
        break;
      default:
        return;
    }
    idlat = nomcampo.replace(tabla + '_id', 'latitud');
    lat = $('#' + idlat);
    idlon = nomcampo.replace(tabla + '_id', 'longitud');
    lon = $('#' + idlon);
    if (idlat !== nomcampo && idlon !== nomcampo && lat.length > 0 && lon.length > 0) {
      y = $.getJSON(root.puntomontaje + "admin/" + modelo + "/" + id + ".json", {
        id: id
      });
      y.done(function(data) {
        var nla, nlo;
        if (data.length > 0) {
          data = data.pop();
        }
        if (+data.latitud !== 0) {
          nla = +data.latitud + Math.random() / 1000 - 0.0005;
          lat.val(nla);
        }
        if (+data.longitud !== 0) {
          nlo = +data.longitud + Math.random() / 1000 - 0.0005;
          return lon.val(nlo);
        }
      });
      return y.fail(function(m1, m2, m3) {
        if (m1.responseText.indexOf("Acceso no autorizado") >= 0) {
          return alert("Se requiere autenticación");
        } else {
          return alert('Problema leyendo ' + tabla + ", id=" + id + ". " + m1 + m2 + m3);
        }
      });
    }
  };

  this.llena_departamento = function($this, root, sincoord) {
    var idcla, iddep, idmun, idpais, pais, x;
    if (sincoord == null) {
      sincoord = false;
    }
    msip_arregla_puntomontaje(root);
    idpais = $this.attr('id');
    iddep = busca_campo_similar(idpais, 'pais', 'departamento');
    idmun = busca_campo_similar(idpais, 'pais', 'municipio');
    idcla = busca_campo_similar(idpais, 'pais', 'clase');
    pais = $this.val();
    if (+pais > 0 && iddep) {
      x = $.getJSON(root.puntomontaje + "admin/departamentos", {
        pais_id: pais
      });
      x.done(function(data) {
        msip_remplaza_opciones_select(iddep, data, true, 'id', 'nombre', true);
        $('#' + iddep).attr('disabled', false);
        $('#' + iddep).trigger('chosen:updated');
        if (idmun) {
          $("#" + idmun + " option[value='']").attr('selected', true);
        }
        if (idmun) {
          $('#' + idmun).attr('disabled', true);
        }
        $('#' + idmun).trigger('chosen:updated');
        if (idcla) {
          $("#" + idcla + " option[value='']").attr('selected', true);
        }
        if (idcla) {
          $('#' + idcla).attr('disabled', true);
        }
        return $('#' + idcla).trigger('chosen:updated');
      });
      x.fail(function(m1, m2, m3) {
        return alert('Problema leyendo Departamentos de ' + pais + ' ' + m1 + ' ', +m2 + ' ' + m3);
      });
      if (sincoord !== true && root.msip_sincoord !== true) {
        return pone_coord(root, 'pais', pais, idpais);
      }
    } else {
      if (iddep) {
        $("#" + iddep).val("");
      }
      if (iddep) {
        $("#" + iddep).attr("disabled", true);
      }
      $('#' + iddep).trigger('chosen:updated');
      if (idmun) {
        $("#" + idmun).val("");
      }
      if (idmun) {
        $("#" + idmun).attr("disabled", true);
      }
      $('#' + idmun).trigger('chosen:updated');
      if (idcla) {
        $("#" + idcla).val("");
      }
      if (idcla) {
        $("#" + idcla).attr("disabled", true);
      }
      return $('#' + idcla).trigger('chosen:updated');
    }
  };

  this.llena_municipio = function($this, root, sincoord) {
    var dep, idcla, iddep, idmun, idpais, x;
    if (sincoord == null) {
      sincoord = false;
    }
    msip_arregla_puntomontaje(root);
    iddep = $this.attr('id');
    idpais = busca_campo_similar(iddep, 'departamento', 'pais');
    idmun = busca_campo_similar(iddep, 'departamento', 'municipio');
    idcla = busca_campo_similar(iddep, 'departamento', 'clase');
    dep = $this.val();
    if (+dep > 0 && idmun !== '') {
      x = $.getJSON(root.puntomontaje + "admin/municipios", {
        departamento_id: dep
      });
      x.done(function(data) {
        msip_remplaza_opciones_select(idmun, data, true, 'id', 'nombre', true);
        if (idmun) {
          $("#" + idmun).attr("disabled", false);
        }
        $('#' + idmun).trigger('chosen:updated');
        if (idcla) {
          $("#" + idcla + " option[value='']").attr('selected', true);
        }
        if (idcla) {
          $("#" + idcla).attr("disabled", true);
        }
        return $('#' + idcla).trigger('chosen:updated');
      });
      x.fail(function(m1, m2, m3) {
        return alert('Problema leyendo Municipios de ' + dep + ' ' + m1 + ' ', +m2 + ' ' + m3);
      });
      if (sincoord !== true && root.msip_sincoord !== true) {
        return pone_coord(root, 'departamento', dep, iddep);
      }
    } else {
      if (idmun) {
        $("#" + idmun).val("");
      }
      if (idmun) {
        $("#" + idmun).attr("disabled", true);
      }
      $('#' + idmun).trigger('chosen:updated');
      if (idcla) {
        $("#" + idcla).val("");
      }
      if (idcla) {
        $("#" + idcla).attr("disabled", true);
      }
      return $('#' + idcla).trigger('chosen:updated');
    }
  };

  this.llena_clase = function($this, root, sincoord) {
    var idcla, iddep, idmun, idpais, msip_arregla_puntomontaje, mun, x;
    if (sincoord == null) {
      sincoord = false;
    }
    msip_arregla_puntomontaje = root;
    idmun = $this.attr('id');
    idpais = busca_campo_similar(idmun, 'municipio', 'pais');
    iddep = busca_campo_similar(idmun, 'municipio', 'departamento');
    idcla = busca_campo_similar(idmun, 'municipio', 'clase');
    mun = $this.val();
    if (+mun > 0 && idcla !== '') {
      x = $.getJSON(root.puntomontaje + "admin/clases", {
        municipio_id: mun
      });
      x.done(function(data) {
        msip_remplaza_opciones_select(idcla, data, true, 'id', 'nombre', true);
        if (idcla) {
          $("#" + idcla).attr("disabled", false);
        }
        return $('#' + idcla).trigger('chosen:updated');
      });
      x.fail(function(m1, m2, m3) {
        return alert('Problema leyendo Clase ' + x + m1 + m2 + m3);
      });
      if (sincoord !== true && root.msip_sincoord !== true) {
        return pone_coord(root, 'municipio', mun, idmun);
      }
    } else if (idcla !== '') {
      if (idcla) {
        $("#" + idcla + " option[value='']").attr('selected', true);
      }
      if (idcla) {
        $("#" + idcla).attr("disabled", true);
      }
      return $('#' + idcla).trigger('chosen:updated');
    }
  };

  this.pone_tipourbano = function($this) {
    var cla, idcla, idts;
    idcla = $this.attr('id');
    idts = busca_campo_similar(idcla, 'clase', 'tsitio');
    cla = $this.val();
    if (+cla > 0 && idcla !== '') {
      return $("#" + idts + " option[value='2']").prop('selected', true);
    }
  };

}).call(this);

// Cambia id de los campoubi relacionados con el control de ubicacionpre
// expandible en 2 filas, que tengan id 0.
function msip_ubicacionpre_expandible_cambia_ids(campoubi, cocoonid) {
  control = $('#ubicacionpre-' + campoubi + '-0').parent()
  control.find('#ubicacionpre-' + campoubi + '-0').attr('id', 
    'ubicacionpre-' + campoubi + '-'+ cocoonid)
  control.find('#resto-' + campoubi + '-0').attr('id', 
    'resto-' + campoubi + '-'+ cocoonid)
  control.find('#restocp-' + campoubi + '-0').attr('id', 
    'restocp-' + campoubi + '-'+ cocoonid)
  b = control.find('button[data-bs-target$=' + campoubi + '-0]')
  console.log(b.attr('data-bs-target'))
  b.attr('data-bs-target', 
    '#resto-' + campoubi + '-' + cocoonid + ',#restocp-' + campoubi + '-' + 
    cocoonid)
}


function msip_ubicacionpre_expandible_maneja_evento_busca_lugar(e) {
  root = window
  ubicacionpre = $(this).closest('.ubicacionpre')
  if (ubicacionpre.length != 1) {
    alert('No se encontró ubicacionpre para ' + 
      $(this).attr('id'))
  }

  epais = ubicacionpre.find('[id$=pais_id]')
  pais = +epais.val()
  dep = +ubicacionpre.find('[id$=departamento_id]').val()
  mun = +ubicacionpre.find('[id$=municipio_id]').val()
  clas = +ubicacionpre.find('[id$=clase_id]').val()
  ubi = [pais, dep, mun, clas]
  msip_ubicacionpre_expandible_busca_lugar($(this), ubi)
}


function msip_ubicacionpre_expandible_busca_lugar(s, ubi) {
  root = window
  msip_arregla_puntomontaje(root)
  cnom = s.attr('id')
  v = $("#" + cnom).data('autocompleta')
  if (v != 1 && v != "no"){
    $("#" + cnom).data('autocompleta', 1)
    // Buscamos un div con clase div_ubicacionpre dentro del cual
    // están tanto el campo ubicacionpre_id como el campo
    // ubicacionpre_texto 
    ubipre = s.closest('.div_ubicacionpre')
    if (typeof ubipre == 'undefined'){
      alert('No se ubico .div_ubicacionpre')
      return
    }
    if ($(ubipre).find("[id$='ubicacionpre_id']").length != 1) {
      alert('Dentro de .div_ubicacionpre no se ubicó ubicacionpre_id')
      return
    }
    if ($(ubipre).find("[id$='_lugar']").length != 1) {
      alert('Dentro de .div_ubicacionpre no se ubicó _lugar')
      return
    }
    var campo = document.querySelector("#" + cnom)
    // Cada vez que llegue quitar eventlistener si ya fue inicializado
    var n = new AutocompletaAjaxCampotexto(campo, root.puntomontaje + 
      "ubicacionespre_lugar.json" + '?pais=' + ubi[0] + 
      '&dep=' + ubi[1] + '&mun=' + ubi[2] + '&clas=' + ubi[3] + '&', 
      'fuente-lugar', function (event, nomop, idop, otrosop) { 
        msip_ubicacionpre_expandible_autocompleta_lugar(otrosop['clase_id'],
          otrosop['tsitio_id'], otrosop['lugar'], 
          otrosop['sitio'], otrosop['latitud'], otrosop['longitud'], 
          ubipre, window)
        event.stopPropagation()
        event.preventDefault()
      }.bind(n)
    )
    n.iniciar()
  }
  return
}

function msip_ubicacionpre_expandible_autocompleta_lugar(clase_id, tsit, lug, sit, lat, lon, ubipre, root){
  msip_arregla_puntomontaje(root)
  ubipre.parent().find('[id$=_clase_id]').val(clase_id).trigger('chosen:updated')
  ubipre.find('[id$=_lugar]').val(lug)
  ubipre.find('[id$=_sitio]').val(sit)
  if (lat != 0 && lat != null){
  ubipre.find('[id$=_latitud]').val(lat)
  }
  if (lon != 0 && lon != null){
  ubipre.find('[id$=_longitud]').val(lon)
  }
  if (tsit != null){
    ubipre.find('[id$=_tsitio_id]').val(tsit).trigger('chosen:updated')
  }
  $(document).trigger("msip:autocompletada-ubicacionpre")
  return
}


function deshabilita_otros_sinohaymun(e, campoubi){
  ubp = $(e.target).closest('.ubicacionpre')
  lugar = ubp.find('[id$='+campoubi+'_lugar]')
  sitio = ubp.find('[id$='+campoubi+'_sitio]')
  tsitio = ubp.find('[id$='+campoubi+'_tsitio_id]')
  latitud = ubp.find('[id$='+campoubi+'_latitud]')
  longitud = ubp.find('[id$='+campoubi+'_longitud]')
  lugar.val("")
  lugar.attr('disabled', true).trigger('chosen:updated')
  sitio.val(null)
  sitio.attr('disabled', true).trigger('chosen:updated')
  tsitio.val(3)
  tsitio.attr('disabled', true).trigger('chosen:updated')
  latitud.val("")
  latitud.attr('disabled', true).trigger('chosen:updated')
  longitud.val("")
  longitud.attr('disabled', true).trigger('chosen:updated')
}

function habilita_otros_sihaymun(e, tipo, campoubi){
  ubp = $(e.target).closest('.ubicacionpre')
  lugar = ubp.find('[id$='+campoubi+'_lugar]')
  sitio = ubp.find('[id$='+campoubi+'_sitio]')
  tsitio = ubp.find('[id$='+campoubi+'_tsitio_id]')
  latitud = ubp.find('[id$='+campoubi+'_latitud]')
  longitud = ubp.find('[id$='+campoubi+'_longitud]')
  if(tipo == 1){
    lugar.attr('disabled', false).trigger('chosen:updated')
    tsitio.attr('disabled', false).trigger('chosen:updated')
  }
  if(tipo == 2){
    sitio.attr('disabled', false).trigger('chosen:updated')
    latitud.attr('disabled', false).trigger('chosen:updated')
    longitud.attr('disabled', false).trigger('chosen:updated')
  }
}


function msip_ubicacionpre_fija_coordenadas(e, campoubi, elemento, ubi_plural){
  ubp = $(e.target).closest('.ubicacionpre')
  latitud = ubp.find('[id$='+campoubi+'_latitud]')
  longitud = ubp.find('[id$='+campoubi+'_longitud]')

  id = Number.parseInt($(elemento).val(), 10) // evita eventual XSS
  root = window
  $.getJSON(root.puntomontaje + "admin/" + ubi_plural +".json", function(o){
    ubi = o.filter(function(item){
      return item.id == id
    })
    if(ubi[0]){
      if(ubi[0].latitud){
        latitud.val(ubi[0].latitud).trigger('chosen:updated')
        longitud.val(ubi[0].longitud).trigger('chosen:updated')
      }
    }else{
      latitud.val(null).trigger('chosen:updated')
      longitud.val(null).trigger('chosen:updated')
    }
  });
}


// iniid Inicio de identificacion por ejemplo 'caso_migracion_attributes'
// campoubi Identificación particular del que se registra por ejemplo 'salida'
//    (teniendo en cuenta que haya campos para el mismo, por ejemplo
//    uno terminado en salida_lugar).
// root Raiz
// fcamdep Función opcional por llamar cuando cambie el departamento
// fcammun Función opcional por llamar cuando cambie el municipio
function msip_ubicacionpre_expandible_registra(iniid, campoubi, root, 
  fcamdep = null, fcammun = null) {
  msip_arregla_puntomontaje(root)

  // Buscador en campo lugar
  $(document).on('focusin', 
    'input[id^=' + iniid + '][id$=_' + campoubi + '_lugar]', 
    msip_ubicacionpre_expandible_maneja_evento_busca_lugar
  )

  // Cambia coordenadas al cambiar pais
  $(document).on('change', 
    '[id^=' + iniid + '][id$=' + campoubi + '_pais_id]', function (evento) {
      msip_ubicacionpre_fija_coordenadas(evento, campoubi, $(this), "paises")
      deshabilita_otros_sinohaymun(evento, campoubi)
    }
  )

  // Cambia coordenadas y deshabilita otros campos al cambiar departamento
  $(document).on('change', 
    '[id^=' + iniid + '][id$=' + campoubi + '_departamento_id]', 
    function (evento) {
      if($(this).val() == "") {
        ubp = $(evento.target).closest('.ubicacionpre')
        let epais = ubp.find('[id$='+campoubi+'_pais_id]')
        msip_ubicacionpre_fija_coordenadas(evento, campoubi, epais, "paises")
      } else {
        msip_ubicacionpre_fija_coordenadas(evento, campoubi, $(this), "departamentos")
      }
      deshabilita_otros_sinohaymun(evento, campoubi)
      if (fcamdep) {
        fcamdep()
      }
    })

  // Cambia coordenadas y habilita otros campos al cambiar municipio
  $(document).on('change', 
    '[id^=' + iniid + '][id$=' + campoubi + '_municipio_id]', 
    function (evento) {
      if($(this).val() == '') {
        ubp = $(evento.target).closest('.ubicacionpre')
        dep = ubp.find('[id$='+campoubi+'_departamento_id]')
        msip_ubicacionpre_fija_coordenadas(evento, campoubi, dep, "departamentos")
        deshabilita_otros_sinohaymun(evento, campoubi)
      }else{
        msip_ubicacionpre_fija_coordenadas(evento, campoubi, $(this), "municipios")
        habilita_otros_sihaymun(evento, 1, campoubi)
      }
      if (fcammun) {
        fcammun()
      }
    })

  // Cambia coordenadas y habilita otros campos al cambiar centro poblado
  $(document).on('change', 
    '[id^=' + iniid + '][id$=' + campoubi + '_clase_id]', 
    function (evento) {
      if($(this).val()==""){
        ubp = $(evento.target).closest('.ubicacionpre')
        mun = ubp.find('[id$='+campoubi+'_municipio_id]')
        msip_ubicacionpre_fija_coordenadas(evento, campoubi, mun, "municipios")
      }else{
        msip_ubicacionpre_fija_coordenadas(evento, campoubi, $(this), "clases")
      }
      habilita_otros_sihaymun(evento, 1, campoubi)
    })

  // Habilita otros campos al cambiar lugar
  $(document).on('change', 
    '[id^=' + iniid + '][id$=' + campoubi + '_lugar]', 
    function (evento) {
      habilita_otros_sihaymun(evento, 2, campoubi)
    }
  )

}
;
'use strict';

// Motor con funciones en Javascript ES6 puro (sin CoffeeScript y sin jQuery)
// Capitalización Camello. Comienzan con Msip sigue Verbo en infinitivo y más.

/*!
 * Serializa valores de un formulario en un arreglo
 * Idea de serializeArray de jQuery, implemantación basada en
 * https://vanillajstoolkit.com/helpers/serializearray/
 * FormData debería dejar esto obsoleto
 **/
function MsipSerializarFormularioEnArreglo(formulario) {
  var arr = [];
  Array.prototype.slice.call(formulario.elements).forEach(function (campo) {
    if (!campo.name || campo.disabled || 
      ['file', 'reset', 'submit', 'button'].indexOf(campo.type) > -1) return;
    if (campo.type === 'select-multiple') {
      Array.prototype.slice.call(campo.options).forEach(function (opcion) {
        if (!opcion.selected) return;
        arr.push({
          name: campo.name,
          value: opcion.value
        });
      });
      return;
    }
    if (['checkbox', 'radio'].indexOf(campo.type) >-1 && !campo.checked) return;
    arr.push({
      name: campo.name,
      value: campo.value
    });
  });
  return arr;
};



/* Convierte arreglo (como el producido por MsipSerializarFormularioEnArreglo)
 * en una cadena apta para enviar consulta.
 * Con base en jQuery.param
 * https://github.com/jquery/jquery-dist/blob/main/src/serialize.js
 */
function MsipConvertirArregloAParam(a) {
  if ( a == null || !Array.isArray( a ) ) {
    return "";
  }

  s = [];
  for(var i = 0; i < a.length; i++) {
    s[ s.length ] = encodeURIComponent( a[i].name ) + "=" +
      encodeURIComponent( a[i].value == null ? "" : a[i].value );

  }

  // Retorna serialización resultante
  return s.join( "&" );
};


/** Enviar AJAX
 * @param url Url
 * @param datos Cuerpo
 */
function MsipEnviarAjax(url, datos, metodo='GET', tipo='script', 
  alertaerror=true) {
  var root =  window
  var t = Date.now()
  var d = -1
  if (root.MsipEnviarAjaxTestigo) {
    d = (t - root.MsipEnviarAjaxTestigo)/1000
  }
  root.MsipEnviarAjaxTestigo = t
  if (d == -1 || d > 2) {
    var enc = {}
    if (document.querySelector('meta[name="csrf-token"]') != null) {
      enc['X-CSRF-Token'] = document.
        querySelector('meta[name="csrf-token"]').getAttribute('content')
    }
    if (tipo == 'script') {
      // https://stackoverflow.com/questions/44803944/can-i-run-a-js-script-from-another-using-fetch
      const promesaScript = new Promise((resolve, reject) => {
          const script = document.createElement('script');
          document.body.appendChild(script);
          script.onload = resolve;
          script.onerror = reject;
          script.async = true;
          script.src = url;
      });

      promesaScript
        .then(resultado => {
          console.log('Éxito:', resultado);
        })
        .catch(error => {
          console.error('Error:', error);
          if (alertaerror) {
            alert('Error: el servicio respondió: ' + error)
          }
        });

    } else {
      if (tipo == 'json') {
        enc['Content-Type'] = 'application/json'
      } else if (tipo == 'texto') {
        enc['Content-Type'] = 'text/plain'
      } else if (tipo == 'html') {
        enc['Content-Type'] = 'text/html'
      } else {
        alert('Tipo desconocido: ' + tipo)
        return;
      }

      fetch(url, {
        method: metodo,
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: enc,
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        body: datos
      })
        .then(respuesta => respuesta.json())
        .then(resultado => {
          console.log('Éxito:', resultado);
        })
        .catch(error => {
          console.error('Error:', error);
          if (alertaerror) {
            alert('Error: el servicio respondió: ' + error)
          }
        });
    }
  }
  return
  }


/** Envia datos de un formulario empleando AJAX
 * @param f Formulario
 */
function MsipEnviarFormularioAjax(f, metodo='GET', tipo='script', 
  alertaerror=true, vcommit='Enviar', agenviarautom = true) {

  var a = f.getAttribute('action')
  const datosFormulario = new FormData(f);
  datosFormulario.append('commit', vcommit)
  if (agenviarautom) {
    datosFormulario.push('_msip_enviarautomatico', 1)
  }
  MsipEnviarAjax(a, datosFormulario, metodo, tipo, alertaerror)
}

function MsipCalcularCambiosParaBitacora() {
  let bitacora = document.querySelector("input.bitacora_cambio");
  if (bitacora == null) {
    return { vacio: false };
  }
  window.bitacora_estado_final_formulario = MsipSerializarFormularioEnArreglo(
    bitacora.closest("form")
  );
  if (typeof window.bitacora_estado_inicial_formulario != "object") {
    return { vacio: false };
  }
  let cambio = {};
  let di = {};
  window.bitacora_estado_inicial_formulario.forEach(
    (v) => di[v.name] = v.value
  );
  let df = {};
  window.bitacora_estado_final_formulario.forEach((v) => {
    df[v.name] = v.value;
    if (typeof di[v.name] == "undefined") {
      cambio[v.name] = [null, v.value];
    }
  });
  for (const i in di) {
    if (typeof df[i] == "undefined") {
      cambio[i] = [di[i], null];
    } else if (df[i] != di[i] && i.search(/\[bitacora_cambio\]/) < 0) {
      cambio[i] = [di[i], df[i]];
    }
  }
  return cambio;
}


/*
 * Con AJAX actualiza formulario, espera recibir formulario guardado
 * para repintar áreas identificadas con listaIdsRepintar y llamar
 * la retrollamada.
 *
 * Se espera que en rails la función update, maneje PATCH y request.xhr?
 * para no ir a hacer redirect_to con lo proveniente de un XHR 
 * (ver ejemplo en lib/sip/concerns/controllers/modelos_controller.rb)
 *
 * @param listaIdsRepintar Lista de ids de elementos por repintar
 *   Si hay uno llamado errores no vacio después de pintar detiene
 *   con mensaje de error.
 * @param retrollamada_exito Función por llamar en caso de éxito
 * @parama argumentos_exito Por pasar a la función retrollamada_exito (se 
 * sugiere que sea un registro).
 * @param retrollamada_falla Función por llamar en caso de falla
 * @parama argumentos_falla Por pasar a la función retrollamada_falla (se 
 *  sugiere que sea un registro).
 */
function MsipGuardarFormularioYRepintar(listaIdsRepintar, retrollamada_exito, 
  argumentos_exito, retrollamada_falla = null, argumentos_falla = null) {
  if (document.body.style.cursor == 'wait') {
    alert('Hay un procedimiento en curso, por favor espere a que termine')
    return
  }
  document.body.style.cursor = 'wait'
  let formulario = document.querySelector('form')
  if (formulario == null) {
    document.body.style.cursor = 'default'
    alert('** MsipGuardarFormularioYRepintar: No se encontró formulario')
    if (retrollamada_falla != null) {
      retrollamada_falla(argumentos_falla)
    }
    return
  }
  let datos = new FormData(formulario);
  datos.set('commit', 'Enviar')
  datos.set('siguiente', 'editar')
  datos.set('_msip_enviarautomatico_y_repinta', 'editar')
  let paramUrl = new URLSearchParams(datos).toString()
  document.getElementById('errores').innerText=''
  window.Rails.ajax({
    type: 'PATCH',
    url: formulario.getAttribute('action'),
    data: datos,
    dataType: 'html',
    success: (resp, estado, xhr) => {
      document.body.style.cursor = 'default'
      let hayErrores = false
      listaIdsRepintar.forEach((idfrag) => {
        let f = document.getElementById(idfrag)
        let nf = resp.getElementById(idfrag)
        if (nf) {
          f.innerHTML = nf.innerHTML
          if (idfrag === 'errores' && nf.innerHTML.trim() !== '') {
            hayErrores = true
          }
        }
      })
      if (hayErrores) {
        document.body.style.cursor = 'default'
        alert('Revise los errores de validación, resuelvalos y vuelva a intentar')
        if (retrollamada_falla != null) {
          retrollamada_falla(argumentos_falla)
        }
        return
      }
      retrollamada_exito(argumentos_exito)
    },
    error: (req, estado, xhr) => {
      document.body.style.cursor = 'default'
      window.alert('No se pudo guardar formulario.')
      if (retrollamada_falla != null) {
        retrollamada_falla(argumentos_falla)
      }
      return
    }
  })
}

function MsipIniciar() {

  MsipAutocompletaAjaxContactos.iniciar()
  MsipAutocompletaAjaxFamiliares.iniciar()

}

// MACHETE PARA MEDIO SOPORTAR PAQUETES ESTILO COMMONJS EN NAVEGADOR
// REQUIRE CAMBIAR USOS QUE SE HACIAN DE root = exports PARA QUE SEAN root =
// window
exports = {}
module = { exports: exports}
;
Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i]; return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var MsipAutocompletaAjaxContactos = (function () {
  function MsipAutocompletaAjaxContactos() {
    _classCallCheck(this, MsipAutocompletaAjaxContactos);
  }

  // Queriamos hacer dentro de MsipAutocompletaAjaxConactos static
  // claseEnvoltura = 'campos_persona' pero la versión de bable usada por babel-transpiler, usado por sprockets4 no lo soporta así que:
  _createClass(MsipAutocompletaAjaxContactos, null, [{
    key: 'operarElegida',

    /* No usamos constructor ni this porque en operaElegida sería
     * del objeto AutocompletaAjaxExpreg y no esta clase.
     * Más bien en esta todo static
     */

    // Elije una persona en autocompletación
    value: function operarElegida(eorig, cadpersona, id, otrosop) {
      var root = window;
      msip_arregla_puntomontaje(root);
      var cs = id.split(';');
      var idPersona = cs[0];
      if ([].concat(_toConsumableArray(document.querySelector('#orgsocial_persona').querySelectorAll('[id$=_attributes_id]'))).filter(function (e) {
        return e.value == idPersona;
      }).length > 0) {
        window.alert("La misma persona ya está en el listado de contactos");
        return;
      }
      var d = '&persona_id=' + idPersona;
      d += '&ac_orgsocial_persona=true';
      var a = root.puntomontaje + 'personas/datos';

      window.Rails.ajax({
        type: 'GET',
        url: a,
        data: d,
        success: function success(resp, estado, xhr) {
          var divcp = eorig.target.closest('.' + MsipAutocompletaAjaxContactos.claseEnvoltura);
          if (divcp == null) {
            alert('No se encontró elmento con clase ' + MsipAutocompletaAjaxContactos.claseEnvoltura);
          }
          divcp.querySelector('[id$=_attributes_id]').value = resp.id;
          divcp.querySelector('[id$=_attributes_nombres]').value = resp.nombres;
          divcp.querySelector('[id$=_attributes_apellidos]').value = resp.apellidos;
          divcp.querySelector('[id$=_attributes_sexo]').value = resp.sexo;
          var tdocid = divcp.querySelector('[id$=_attributes_tdocumento_id]');
          if (tdocid != null) {
            var idop = null;
            tdocid.childNodes.forEach(function (o) {
              if (typeof o.innerText === 'string' && o.innerText === resp.tdocumento) {
                idop = o.value;
              }
            });
            if (idop != null) {
              tdocid.value = idop;
            }
          }
          var numdoc = divcp.querySelector('[id$=_numerodocumento]');
          if (numdoc != null) {
            numdoc.value = resp.numerodocumento;
          }
          var anionac = divcp.querySelector('[id$=_anionac]');
          if (anionac != null) {
            anionac.value = resp.anionac;
          }
          var mesnac = divcp.querySelector('[id$=_mesnac]');
          if (mesnac != null) {
            mesnac.value = resp.mesnac;
          }
          var dianac = divcp.querySelector('[id$=_dianac]');
          if (dianac != null) {
            dianac.value = resp.dianac;
          }
          var cargo = divcp.querySelector('[id$=_cargo]');
          if (cargo != null && typeof resp.cargo != 'undefined') {
            cargo.value = resp.cargo;
          }
          var correo = divcp.querySelector('[id$=_correo]');
          if (correo != null && typeof resp.correo != 'undefined') {
            correo.value = resp.correo;
          }
          eorig.target.setAttribute('data-autocompleta', 'no');
          eorig.target.removeAttribute('list');
          var sel = document.getElementById(MsipAutocompletaAjaxContactos.idDatalist);
          sel.innerHTML = '';
          document.dispatchEvent(new Event('msip:autocompletado-contacto'));
        },
        error: function error(resp, estado, xhr) {
          window.alert('Error con ajax ' + resp);
        }
      });
    }
  }, {
    key: 'iniciar',
    value: function iniciar() {
      console.log("MsipAutocompletaAjaxContactos msip");
      var url = window.puntomontaje + 'personas.json';
      var contactos = new window.AutocompletaAjaxExpreg([/^orgsocial_orgsocial_persona_attributes_[0-9]*_persona_attributes_nombres$/], url, MsipAutocompletaAjaxContactos.idDatalist, MsipAutocompletaAjaxContactos.operarElegida);
      contactos.iniciar();
    }
  }]);

  return MsipAutocompletaAjaxContactos;
})();

exports['default'] = MsipAutocompletaAjaxContactos;
MsipAutocompletaAjaxContactos.claseEnvoltura = 'campos_persona';
MsipAutocompletaAjaxContactos.idDatalist = 'fuente-contactos-orgsocial';
module.exports = exports['default'];
Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var MsipAutocompletaAjaxFamiliares = (function () {
  function MsipAutocompletaAjaxFamiliares() {
    _classCallCheck(this, MsipAutocompletaAjaxFamiliares);
  }

  // Sobrellevamos imposibilidad de hacer static claseEnvoltura y
  // static idDatalist dentro de la clase MsipAutocompletaAjaxFamiliares asi:
  _createClass(MsipAutocompletaAjaxFamiliares, null, [{
    key: 'operarElegida',

    /* No usamos constructor ni this porque en operaElegida sería
     * del objeto AutocompletaAjaxExpreg y no esta clase.
     * Más bien en esta todo static
     */

    // Elije una persona en autocompletación
    value: function operarElegida(eorig, cadpersona, id, otrosop) {
      var root = window;
      msip_arregla_puntomontaje(root);
      var cs = id.split(';');
      var idPersona = cs[0];
      var divcpf = eorig.target.closest('.' + MsipAutocompletaAjaxFamiliares.claseEnvoltura);
      divcpf.querySelector('.persona_persona_trelacion1_personados_id > input').value = idPersona;
      divcpf.querySelector('.persona_persona_trelacion1_personados_nombres > input').value = "";
      divcpf.querySelector('.persona_persona_trelacion1_personados_apellidos > input').value = "";
      divcpf.querySelector('input[value=Actualizar]').click();
    }
  }, {
    key: 'iniciar',
    value: function iniciar() {
      console.log("MsipAutocompletaAjaxFamiliares");
      var url = window.puntomontaje + 'personas.json';
      var asistentes = new window.AutocompletaAjaxExpreg([/^persona_persona_trelacion1_attributes_[0-9]*_personados_attributes_nombres$/], url, MsipAutocompletaAjaxFamiliares.idDatalist, MsipAutocompletaAjaxFamiliares.operarElegida);
      asistentes.iniciar();
    }
  }]);

  return MsipAutocompletaAjaxFamiliares;
})();

exports['default'] = MsipAutocompletaAjaxFamiliares;
MsipAutocompletaAjaxFamiliares.claseEnvoltura = 'nested-fields';
MsipAutocompletaAjaxFamiliares.idDatalist = 'fuente-familiares';
module.exports = exports['default'];
(function() {
  this.msip_remplaza_opciones_select = function(idsel, nuevasop, usachosen, cid, cetiqueta, opvacia) {
    var nh, s, sel;
    if (usachosen == null) {
      usachosen = false;
    }
    if (cid == null) {
      cid = 'id';
    }
    if (cetiqueta == null) {
      cetiqueta = 'nombre';
    }
    if (opvacia == null) {
      opvacia = false;
    }
    s = $("#" + idsel);
    if (s.length !== 1) {
      alert('msip_remplaza_opciones_select: no se encontró ' + idsel);
      return;
    }
    sel = s.val();
    nh = '';
    if (opvacia) {
      nh = "<option value=''></option>";
    }
    nuevasop.forEach(function(v) {
      var id, tx;
      id = v[cid];
      nh = nh + "<option value='" + id + "'";
      if (id !== '' && sel !== null && (('' + id) === ('' + sel) || sel.indexOf('' + id) >= 0)) {
        nh = nh + ' selected';
      }
      tx = v[cetiqueta];
      return nh = nh + ">" + tx + "</option>";
    });
    s.html(nh);
    if (usachosen) {
      $('#' + idsel).trigger("chosen:updated");
    }
  };

  this.msip_escapaHtml = function(cadena) {
    return cadena.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;");
  };

  this.msip_actualiza_cuadros_seleccion_dependientes = function(idfuente, posfijo_id_fuente, posfijo_etiqueta_fuente, seldestino, opvacia) {
    var lobj, nuevasop;
    if (opvacia == null) {
      opvacia = false;
    }
    nuevasop = [];
    lobj = $('#' + idfuente + ' .nested-fields[style!="display: none;"]');
    lobj.each(function(k, v) {
      var etiqueta, id;
      id = $(v).find('input[id$=' + posfijo_id_fuente + ']').val();
      etiqueta = $(v).find('input[id$=' + posfijo_etiqueta_fuente + ']').val();
      return nuevasop.push({
        id: id,
        etiqueta: etiqueta
      });
    });
    seldestino.forEach(function(sel) {
      return $(sel).each(function(i, r) {
        var conch;
        conch = $(r).hasOwnProperty('chosen');
        return msip_remplaza_opciones_select($(r).attr('id'), nuevasop, conch, 'id', 'etiqueta', opvacia);
      });
    });
  };

  this.msip_actualiza_cuadros_seleccion_dependientes_fun_etiqueta = function(idfuente, posfijo_id_fuente, fun_etiqueta, seldestino, opvacia) {
    var lobj, nuevasop;
    if (opvacia == null) {
      opvacia = false;
    }
    nuevasop = [];
    lobj = $('#' + idfuente + ' .nested-fields[style!="display: none;"]');
    lobj.each(function(k, v) {
      var etiqueta, id;
      id = $(v).find('input[id$=' + posfijo_id_fuente + ']').val();
      etiqueta = fun_etiqueta($(v));
      return nuevasop.push({
        id: id,
        etiqueta: etiqueta
      });
    });
    seldestino.forEach(function(sel) {
      return $(sel).each(function(i, r) {
        var conch;
        conch = $(r).hasOwnProperty('chosen');
        return msip_remplaza_opciones_select($(r).attr('id'), nuevasop, conch, 'id', 'etiqueta', opvacia);
      });
    });
  };

  this.msip_intenta_eliminar_fila = function(fila, prefijo_url, seldep) {
    var bid, d, ide, num, purl, root, t;
    t = Date.now();
    d = -1;
    if (window.ajax_t) {
      d = (t - window.ajax_t) / 1000;
    }
    window.ajax_t = t;
    if (d === -1 || d > 2) {
      bid = fila.find('input[id$=_id]');
      if (bid.length !== 1) {
        return false;
      }
      ide = +$(bid[0]).val();
      if (seldep !== null) {
        num = 0;
        seldep.forEach(function(sel) {
          return $(sel + ' option:selected').each(function() {
            if (+$(this).val() === ide) {
              return num += 1;
            }
          });
        });
        if (num > 0) {
          alert('Hay elementos que depende de este (' + num + '). ' + ' Eliminelos antes.');
          return false;
        }
      }
      root = window;
      purl = prefijo_url;
      if (prefijo_url.substr(0, root.puntomontaje.length) !== root.puntomontaje) {
        purl = root.puntomontaje + prefijo_url;
      }
      $.ajax({
        url: purl + ide,
        type: 'DELETE',
        dataType: 'json',
        beforeSend: (function(xhr) {
          return xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
        }),
        success: (function(response) {
          return $(fila).remove();
        }),
        error: (function(response) {
          if (response.status !== 0 && response.responseText !== '') {
            return alert('Error: el servicio respondio con: ' + response.status + '\n' + response.responseText);
          }
        })
      });
    }
    return true;
  };

  if (typeof String.prototype.endsWith !== 'function') {
    String.prototype.endsWith = function(suffix) {
      return this.indexOf(suffix, this.length - suffix.length) !== -1;
    };
  }

  this.msip_meses = ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic'];

  this.msip_retorna_fecha_localizada = function(fecha, formato_fecha) {
    var fecha_sep, mes_nom, mes_nom_cap, pc, pcmayus, resto;
    fecha_sep = msip_partes_fecha_localizada(fecha, formato_fecha);
    mes_nom = this.msip_meses[fecha_sep[1] - 1];
    pc = mes_nom.substring(0, 1);
    pcmayus = pc.toUpperCase();
    resto = mes_nom.substring(1);
    mes_nom_cap = pcmayus + resto;
    return fecha_sep[2].toString() + '/' + mes_nom_cap + '/' + fecha_sep[0].toString();
  };

  this.msip_partes_fecha_localizada = function(fecha, formato_fecha) {
    var anio, dia, mes, nmes;
    if (formato_fecha === 'dd/M/yyyy' || formato_fecha === 'dd-M-yyyy') {
      anio = +fecha.slice(7, 11);
      dia = +fecha.slice(0, 2);
      nmes = fecha.slice(3, 6);
      if (typeof nmes !== 'undefined' && this.msip_meses.includes(nmes.toLowerCase())) {
        mes = this.msip_meses.indexOf(nmes.toLowerCase()) + 1;
      } else {
        mes = 6;
      }
    } else if (typeof fecha === 'string') {
      anio = +fecha.slice(0, 4);
      mes = +fecha.slice(5, 7);
      dia = +fecha.slice(8, 10);
    } else {
      anio = 1900;
      mes = 1;
      dia = 1;
    }
    return [anio, mes, dia];
  };

  this.fecha_valida = function(text) {
    var comp, d, date, m, y;
    date = Date.parse(text);
    if (isNaN(date)) {
      return false;
    }
    comp = text.split('-');
    if (comp.length !== 3) {
      return false;
    }
    y = parseInt(comp[0], 10);
    m = parseInt(comp[1], 10);
    d = parseInt(comp[2], 10);
    date = new Date(y, m - 1, d);
    return date.getFullYear() === y && date.getMonth() + 1 === m && date.getDate() === d;
  };

  this.msip_ajax_recibe_json = function(root, ruta, datos, funproc) {
    var d, rutac, t;
    msip_arregla_puntomontaje(root);
    t = Date.now();
    d = -1;
    if (root.msip_ajax_recibe_json_t) {
      if (root.msip_ajax_recibe_json_t[ruta]) {
        d = (t - root.msip_ajax_recibe_json_t[ruta]) / 1000;
      }
    } else {
      root.msip_ajax_recibe_json_t = {};
    }
    root.msip_ajax_recibe_json_t[ruta] = t;
    if (d === -1 || d > 2) {
      rutac = root.puntomontaje + ruta + ".json";
      $.ajax({
        url: rutac,
        data: datos,
        dataType: 'json',
        method: 'GET'
      }).fail(function(jqXHR, texto) {
        return alert('Error - ' + texto);
      }).done(function(e, r) {
        return funproc(root, e);
      });
    }
    return true;
  };

  this.msip_envia_ajax_datos_ruta_y_pinta = function(ruta, datos, selresp, selelem, metodo, tipo, concsrf) {
    var d, root, rutac, t, token;
    if (metodo == null) {
      metodo = 'GET';
    }
    if (tipo == null) {
      tipo = 'html';
    }
    if (concsrf == null) {
      concsrf = false;
    }
    root = window;
    t = Date.now();
    d = -1;
    if (root.msip_envia_ajax_t) {
      d = (t - root.msip_envia_ajax_t) / 1000;
    }
    root.msip_envia_ajax_t = t;
    if (d === -1 || d > 2) {
      msip_arregla_puntomontaje(root);
      token = $('meta[name="csrf-token"]').attr('content');
      rutac = root.puntomontaje + ruta + ".js";
      $.ajax({
        url: rutac,
        data: datos,
        dataType: tipo,
        beforeSend: (function(xhr) {
          if (concsrf) {
            return xhr.setRequestHeader('X-CSRF-Token', token);
          }
        }),
        method: metodo
      }).fail(function(jqXHR, texto) {
        return alert("Error con ajax a " + rutac + ": " + texto);
      }).done(function(e, r) {
        if (selresp !== '' && selelem !== '') {
          t = $(e).find(selresp)[0];
          if (selresp === selelem) {
            $(selelem).replaceWith(t);
          } else {
            $(selelem).html(t);
            $('[data-behaviour~=datepicker]').datepicker({
              format: root.formato_fecha,
              autoclose: true,
              todayHighlight: true,
              language: 'es'
            });
          }
        }
      });
    }
  };

  this.msip_enviarautomatico_formulario_y_repinta = function(idf, listaidsrempl, metodo, alertaerror, vcommit) {
    var a, d, dat, f, root, t, vd;
    if (metodo == null) {
      metodo = 'GET';
    }
    if (alertaerror == null) {
      alertaerror = true;
    }
    if (vcommit == null) {
      vcommit = 'Enviar';
    }
    root = window;
    t = Date.now();
    d = -1;
    if (root.msip_enviarautomatico_t) {
      d = (t - root.msip_enviarautomatico_t) / 1000;
    }
    root.msip_enviarautomatico_t = t;
    f = $('form[id=' + idf + ']');
    root.msip_enviarautomatico_idf = idf;
    root.msip_enviarautomatico_l = listaidsrempl;
    if (f.length === 1 && (d === -1 || d > 2)) {
      a = f.attr('action');
      vd = f.serializeArray();
      vd.push({
        name: 'commit',
        value: vcommit
      });
      vd.push({
        name: '_msip_enviarautomatico',
        value: 1
      });
      vd.push({
        name: '_msip_enviarautomatico_y_repinta',
        value: 1
      });
      dat = $.param(vd);
      if (!root.dant || root.dant !== d) {
        root.dant = d;
        $.ajax({
          url: a,
          data: dat,
          method: metodo,
          dataType: 'html',
          beforeSend: (function(xhr) {
            return xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
          }),
          error: (function(response) {
            if (alertaerror && response.status !== 0 && response.responseText !== '') {
              return alert('Error: el servicio respondió: ' + response.status + '\n' + response.responseText);
            }
          }),
          success: (function(e, r, j) {
            var listaidsremp;
            listaidsremp = root.msip_enviarautomatico_l;
            listaidsremp.forEach(function(idf) {
              t = $(e).find('#' + idf)[0];
              return $('#' + idf).html(t.innerHTML);
            });
          })
        });
      }
    }
  };

  this.msip_enviarautomatico_formulario = function(f, metodo, tipo, alertaerror, vcommit, agenviarautom) {
    var a, d, dat, root, t, vd;
    if (metodo == null) {
      metodo = 'GET';
    }
    if (tipo == null) {
      tipo = 'script';
    }
    if (alertaerror == null) {
      alertaerror = true;
    }
    if (vcommit == null) {
      vcommit = 'Enviar';
    }
    if (agenviarautom == null) {
      agenviarautom = true;
    }
    root = window;
    t = Date.now();
    d = -1;
    if (root.msip_enviarautomatico_t) {
      d = (t - root.msip_enviarautomatico_t) / 1000;
    }
    root.msip_enviarautomatico_t = t;
    if (d === -1 || d > 2) {
      a = f.attr('action');
      vd = f.serializeArray();
      vd.push({
        name: 'commit',
        value: vcommit
      });
      if (agenviarautom) {
        vd.push({
          name: '_msip_enviarautomatico',
          value: 1
        });
      }
      dat = $.param(vd);
      if (!root.dant || root.dant !== d) {
        root.dant = d;
        $.ajax({
          url: a,
          data: dat,
          method: metodo,
          dataType: tipo,
          beforeSend: (function(xhr) {
            return xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
          }),
          error: (function(response) {
            if (alertaerror && response.status !== 0 && response.responseText !== '') {
              return alert('Error: el servicio respondió: ' + response.status + '\n' + response.responseText);
            }
          })
        });
      }
    }
  };

  this.msip_completa_enlace = function(elema, idruta, rutagenera) {
    var d, e, f, root;
    if (idruta === null) {
      f = $(elema).closest('form');
    } else {
      f = $("form[action$='" + idruta + "']");
    }
    d = f.serialize();
    d += '&commit=Enviar';
    root = window;
    msip_arregla_puntomontaje(root);
    if ((root.puntomontaje !== '/' || rutagenera[0] !== '/') && rutagenera.substring(0, root.puntomontaje.length) !== root.puntomontaje) {
      rutagenera = root.puntomontaje + rutagenera;
    }
    e = rutagenera + '?' + d;
    return $(elema).attr('href', e);
  };

  this.msip_funcion_tras_AJAX = function(rutajson, params, f, descerr, root) {
    var d, t, x;
    if (root == null) {
      root = window;
    }
    msip_arregla_puntomontaje(root);
    t = Date.now();
    d = -1;
    if (root.msip_funcion_tras_AJAX_t) {
      d = (t - root.msip_funcion_tras_AJAX_t) / 1000;
    }
    root.msip_funcion_tras_AJAX_t = t;
    if (d > 0 && d <= 2) {
      return;
    }
    x = $.getJSON(root.puntomontaje + rutajson + ".json", params);
    x.done(function(res) {
      return f(root, res);
    });
    return x.fail(function(m1, m2, m3) {
      return alert('Problema ' + descerr + '. ' + params + ' ' + m1 + ' ' + m2 + ' ' + m3);
    });
  };

  this.msip_funcion_1p_tras_AJAX = function(rutajson, params, f, p1, descerr, root) {
    var d, t, x;
    if (root == null) {
      root = window;
    }
    msip_arregla_puntomontaje(root);
    t = Date.now();
    d = -1;
    if (root.msip_funcion_1p_tras_AJAX_t) {
      d = (t - root.msip_funcion_1p_tras_AJAX_t) / 1000;
    }
    root.msip_funcion_1p_tras_AJAX_t = t;
    if (d > 0 && d <= 1) {
      return;
    }
    x = $.getJSON(root.puntomontaje + rutajson + ".json", params);
    x.done(function(res) {
      return f(root, res, p1);
    });
    return x.fail(function(m1, m2, m3) {
      return alert('Problema ' + descerr + '. ' + params + ' ' + m1 + ' ' + m2 + ' ' + m3);
    });
  };

  this.msip_cambia_cuadrotexto_AJAX = function(rutajson, params, iddest, descerr, f, root) {
    var d, t, x;
    if (f == null) {
      f = null;
    }
    if (root == null) {
      root = window;
    }
    msip_arregla_puntomontaje(root);
    t = Date.now();
    d = -1;
    if (root.msip_cambia_cuadrotexto_AJAX_t) {
      d = (t - root.msip_cambia_cuadrotexto_AJAX_t) / 1000;
    }
    root.msip_cambia_cuadrotexto_AJAX_t = t;
    if (d > 0 && d <= 2) {
      return;
    }
    x = $.getJSON(root.puntomontaje + rutajson + ".json", params);
    x.done(function(datos) {
      $('#' + iddest).val(datos['res']);
      if (f !== null) {
        return f(root);
      }
    });
    return x.fail(function(m1, m2, m3) {
      return alert('Problema ' + descerr + '. ' + params + ' ' + m1 + ' ' + m2 + ' ' + m3);
    });
  };

  this.msip_elige_opcion_select_con_AJAX = function($elem, rutajson, lid, descerr, f, root) {
    var d, t, val, x;
    if (f == null) {
      f = null;
    }
    if (root == null) {
      root = window;
    }
    msip_arregla_puntomontaje(root);
    t = Date.now();
    d = -1;
    if (root.msip_elige_opcion_select_con_AJAX_t) {
      d = (t - root.msip_elige_opcion_select_con_AJAX_t) / 1000;
    }
    root.msip_elige_opcion_select_con_AJAX_t = t;
    if (d > 0 && d <= 2) {
      return;
    }
    val = $elem.val();
    x = $.getJSON(root.puntomontaje + rutajson + ".json");
    x.done(function(datos) {
      lid.forEach(function(p) {
        return $('#' + p[0]).val(datos[p[1]]);
      });
      if (f !== null) {
        return f(root);
      }
    });
    return x.fail(function(m1, m2, m3) {
      return alert('Problema ' + descerr + '. ' + param + ' ' + m1 + ' ' + m2 + ' ' + m3);
    });
  };

  this.msip_llena_select_con_AJAX = function($elem, idsel, rutajson, nomparam, descerr, root, paramfiltro, cid, cnombre, f) {
    var d, param, t, val, x;
    if (root == null) {
      root = window;
    }
    if (paramfiltro == null) {
      paramfiltro = false;
    }
    if (cid == null) {
      cid = 'id';
    }
    if (cnombre == null) {
      cnombre = 'nombre';
    }
    if (f == null) {
      f = null;
    }
    msip_arregla_puntomontaje(root);
    t = Date.now();
    d = -1;
    if (root.msip_llena_select_con_AJAX_t) {
      d = (t - root.msip_llena_select_con_AJAX_t) / 1000;
    }
    root.msip_llena_select_con_AJAX_t = t;
    if (d > 0 && d <= 2) {
      return;
    }
    val = $elem.val();
    param = {};
    param[nomparam] = val;
    if (cnombre === 'presenta_nombre') {
      param['presenta_nombre'] = 1;
    }
    if (paramfiltro) {
      param = {
        filtro: param
      };
    }
    x = $.getJSON(root.puntomontaje + rutajson, param);
    x.done(function(datos) {
      msip_remplaza_opciones_select(idsel, datos, true, cid, cnombre);
      if (f !== null) {
        return f(root);
      }
    });
    return x.fail(function(m1, m2, m3) {
      return alert('Problema ' + descerr + '. ' + param + ' ' + m1 + ' ' + m2 + ' ' + m3);
    });
  };

  this.msip_llena_select_con_AJAX2 = function(rutajson, params, idsel, descerr, root, cid, cnombre, f, opvacia) {
    var d, pv, rv, t, x;
    if (root == null) {
      root = window;
    }
    if (cid == null) {
      cid = 'id';
    }
    if (cnombre == null) {
      cnombre = 'nombre';
    }
    if (f == null) {
      f = null;
    }
    if (opvacia == null) {
      opvacia = false;
    }
    msip_arregla_puntomontaje(root);
    t = Date.now();
    d = -1;
    if (root.msip_llena_select_con_AJAX2_t) {
      d = (t - root.msip_llena_select_con_AJAX2_t) / 1000;
    }
    root.msip_llena_select_con_AJAX2_t = t;
    rv = "";
    if (root.msip_llena_select_con_AJAX2_rv) {
      rv = root.msip_llena_select_con_AJAX2_rv;
    }
    root.msip_llena_select_con_AJAX2_rv = rutajson;
    pv = {};
    if (root.msip_llena_select_con_AJAX2_pv) {
      pv = root.msip_llena_select_con_AJAX2_pv;
    }
    root.msip_llena_select_con_AJAX2_pv = params;
    if (d > 0 && d <= 2 && rutajson === root.msip_llena_select_con_AJAX2_r && params === root.msip_llena_select_con_AJAX2_p) {
      return;
    }
    x = $.getJSON(root.puntomontaje + rutajson, params);
    x.done(function(datos) {
      msip_remplaza_opciones_select(idsel, datos, true, cid, cnombre, opvacia);
      if (f !== null) {
        return f(root);
      }
    });
    return x.fail(function(m1, m2, m3) {
      return alert('Problema ' + descerr + '. ' + params + ' ' + m1 + ' ' + m2 + ' ' + m3);
    });
  };

  this.busca_gen = function(s, sel_id, fuente) {
    s.autocomplete({
      source: fuente,
      minLength: 2,
      select: function(event, ui) {
        if (ui.item) {
          if (sel_id !== null) {
            $(sel_id).val(ui.item.value);
          }
          s.val(ui.item.label);
          event.stopPropagation();
          return event.preventDefault();
        }
      }
    });
  };

  this.msip_arregla_puntomontaje = function(root) {
    if (typeof root.puntomontaje === 'undefined') {
      root.puntomontaje = '/';
    }
    if (root.puntomontaje[root.puntomontaje.length - 1] !== '/') {
      return root.puntomontaje += '/';
    }
  };

  this.msip_pone_tema = function(root, tema) {
    $('.table-striped>tbody>tr:nth-child(odd)').css('background-color', tema.fondo_lista);
    $('.navbar').css('background-image', 'linear-gradient(' + tema.nav_ini + ', ' + tema.nav_fin + ')');
    $('.navbar-default .navbar-nav>li>a').css('color', tema.nav_fuente);
    $('.navbar-default .navbar-brand').css('color', tema.nav_fuente);
    $('.navbar-light .navbar-nav .nav-link').attr('style', 'color: ' + tema.nav_fuente + ' !important');
    $('.navbar-light .navbar-brand').attr('style', 'color: ' + tema.nav_fuente + ' !important');
    $('.dropdown-menu').css('background-color', tema.nav_fin);
    $('.dropdown-item').css('color', tema.nav_fuente);
    $('.dropdown-item').hover(function() {
      return $(this).css({
        'color': tema.color_flota_subitem_fuente,
        'background-color': tema.color_flota_subitem_fondo
      });
    }, function() {
      return $(this).css({
        'color': tema.nav_fuente,
        'background-color': tema.nav_fin
      });
    });
    $('.alert-success').css('color', tema.alerta_exito_fuente);
    $('.alert-success').css('background-color', tema.alerta_exito_fondo);
    $('.alert-danger').css('color', tema.alerta_problema_fuente);
    $('.alert-danger').css('background-color', tema.alerta_problema_fondo);
    $('.btn').css('background-image', 'linear-gradient(to bottom, ' + tema.btn_accion_fondo_ini + ', ' + tema.btn_accion_fondo_fin + ')');
    $('.btn').css('color', tema.btn_accion_fuente);
    $('.btn-primary').css('background-image', 'linear-gradient(to bottom, ' + tema.btn_primario_fondo_ini + ', ' + tema.btn_primario_fondo_fin + ')');
    $('.btn-primary').css('color', tema.btn_primario_fuente);
    $('.btn-danger').css('background-image', 'linear-gradient(to bottom, ' + tema.btn_peligro_fondo_ini + ', ' + tema.btn_peligro_fondo_fin + ')');
    $('.btn-danger').css('color', tema.btn_peligro_fuente);
    $('body').css('background-color', tema.fondo);
    $('.card').css('background-color', tema.fondo);
    $('.msip-sf-bs-input:not(.form-check-input)').css('background-color', tema.fondo);
    $('.msip-sf-bs-input:not(.form-check-input)').css('color', tema.color_fuente);
    $('.page-link').attr('style', 'background-color: ' + tema.fondo + ' !important');
    $('body').css('color', tema.color_fuente);
    return $('table').css('color', tema.color_fuente);
  };

  this.msip_autocompleta_ubicacionpre = function(etiqueta, id, ubipre, root) {
    msip_arregla_puntomontaje(root);
    ubipre.find('[id$=ubicacionpre_id]').val(id);
    ubipre.find('[id$=ubicacionpre_texto]').val(etiqueta);
    ubipre.find('[id$=ubicacionpre_mundep_texto]').val(etiqueta);
    $(document).trigger("msip:autocompletada-ubicacionpre");
  };

  this.msip_busca_ubicacionpre_mundep = function(s) {
    var cnom, root, ubipre, v;
    root = window;
    msip_arregla_puntomontaje(root);
    cnom = s.attr('id');
    v = $("#" + cnom).data('autocompleta');
    if (v !== 1 && v !== "no") {
      $("#" + cnom).data('autocompleta', 1);
      ubipre = s.closest('.div_ubicacionpre');
      if (typeof ubipre === 'undefined') {
        alert('No se ubico .div_ubicacionpre');
        return;
      }
      if ($(ubipre).find("[id$='ubicacionpre_id']").length !== 1) {
        alert('Dentro de .div_ubicacionpre no se ubicó ubicacionpre_id');
        return;
      }
      if ($(ubipre).find("[id$='ubicacionpre_mundep_texto']").length !== 1) {
        alert('Dentro de .div_ubicacionpre no se ubicó ubicacionpre_mundep_texto');
        return;
      }
      $("#" + cnom).autocomplete({
        source: root.puntomontaje + "ubicacionespre_mundep.json",
        minLength: 2,
        select: function(event, ui) {
          if (ui.item) {
            msip_autocompleta_ubicacionpre(ui.item.value, ui.item.id, ubipre, root);
            event.stopPropagation();
            return event.preventDefault();
          }
        }
      });
    }
  };

  this.msip_busca_ubicacionpre = function(s) {
    var cnom, root, ubipre, v;
    root = window;
    msip_arregla_puntomontaje(root);
    cnom = s.attr('id');
    v = $("#" + cnom).data('autocompleta');
    if (v !== 1 && v !== "no") {
      $("#" + cnom).data('autocompleta', 1);
      ubipre = s.closest('.div_ubicacionpre');
      if (typeof ubipre === 'undefined') {
        alert('No se ubico .div_ubicacionpre');
        return;
      }
      if ($(ubipre).find("[id$='ubicacionpre_id']").length !== 1) {
        alert('Dentro de .div_ubicacionpre no se ubicó ubicacionpre_id');
        return;
      }
      if ($(ubipre).find("[id$='ubicacionpre_texto']").length !== 1) {
        alert('Dentro de .div_ubicacionpre no se ubicó ubicacionpre_texto');
        return;
      }
      $("#" + cnom).autocomplete({
        source: root.puntomontaje + "ubicacionespre.json",
        minLength: 2,
        select: function(event, ui) {
          if (ui.item) {
            msip_autocompleta_ubicacionpre(ui.item.value, ui.item.id, ubipre, root);
            event.stopPropagation();
            return event.preventDefault();
          }
        }
      });
    }
  };

  this.msip_registra_cambios_para_bitacora = function(root) {
    if ($('input.bitacora_cambio').length > 0) {
      root.bitacora_estado_inicial_formulario = $('input.bitacora_cambio').closest('form').serializeArray();
    }
    $(document).on('submit', 'form:has(.bitacora_cambio)', function(e) {
      var cambio, df, di, i;
      root.bitacora_estado_final_formulario = $('input.bitacora_cambio').closest('form').serializeArray();
      cambio = {};
      di = {};
      root.bitacora_estado_inicial_formulario.forEach(function(v) {
        return di[v.name] = v.value;
      });
      df = {};
      root.bitacora_estado_final_formulario.forEach(function(v) {
        df[v.name] = v.value;
        if (typeof di[v.name] === 'undefined') {
          return cambio[v.name] = [null, v.value];
        }
      });
      for (i in di) {
        e = di[i];
        if (typeof df[i] === 'undefined') {
          cambio[i] = [e, null];
        } else if (df[i] !== e && i.search(/\[bitacora_cambio\]/) < 0) {
          cambio[i] = [e, df[i]];
        }
      }
      return $('input.bitacora_cambio').val(JSON.stringify(cambio));
    });
  };

  this.msip_reconocer_decimal_locale_es_CO = function(n) {
    var i, r;
    if (n === "") {
      return 0;
    }
    i = 0;
    r = "";
    while (i < n.length) {
      if (n[i] === ',') {
        r = r + '.';
      }
      if (n[i] >= '0' && n[i] <= '9') {
        r = r + n[i];
      }
      i++;
    }
    return parseFloat(r);
  };

  this.msip_inicializaMotor = function(conenv) {
    if (conenv == null) {
      conenv = true;
    }
    window.puntomontaje = "/minmsip/";
    if (conenv && false) {
      window.puntomontaje = "";
    }
    msip_arregla_puntomontaje(window);
    window.msip_sincoord = false;
    window.formato_fecha = 'dd/M/yyyy';
    return window.msip_idioma_predet = 'es';
  };

  this.msip_prepara_eventos_comunes = function(root, sincoord, conenv) {
    var detieneRotador, iniciaRotador;
    if (sincoord == null) {
      sincoord = false;
    }
    if (conenv == null) {
      conenv = true;
    }
    if (typeof root.formato_fecha === 'undefined') {
      msip_inicializaMotor(conenv);
    }
    $('[data-behaviour~=datepicker]').datepicker({
      format: root.formato_fecha,
      autoclose: true,
      todayHighlight: true,
      language: 'es'
    });
    $('[data-toggle="tooltip"]').tooltip();
    $(document).on('cocoon:after-insert', function(e) {
      $('[data-behaviour~=datepicker]').datepicker({
        format: root.formato_fecha,
        autoclose: true,
        todayHighlight: true,
        language: 'es'
      });
      return $('[data-toggle="tooltip"]').tooltip();
    });
    $('.chosen-select').chosen({
      allow_single_deselect: true,
      no_results_text: 'No hay resultados',
      placeholder_text_single: 'Seleccione una opción',
      placeholder_text_multiple: 'Seleccione algunas opciones',
      width: '100%'
    });
    $('select[data-toggle="tooltip"]').each(function(v, e) {
      var ej, id, ns, t;
      ej = $(e);
      t = ej.attr('data-original-title');
      id = ej.attr('id');
      ns = '#' + id + '_chosen .chosen-choices';
      $(ns).attr('title', t);
      return $(ns).attr('data-toggle', 'tooltip');
    });
    msip_ajax_recibe_json(root, 'temausuario', {}, msip_pone_tema);
    jQuery(function() {
      $("a[rel~=popover], .has-popover").popover();
      return $("a[rel~=tooltip], .has-tooltip").tooltip();
    });
    $(document).on('change', 'select[id$=_pais_id]', function(e) {
      return llena_departamento($(this), root, sincoord);
    });
    $(document).on('change', 'select[id$=_pais_id]', function(e) {
      return llena_departamento($(this), root, sincoord);
    });
    $(document).on('change', 'select[id$=_departamento_id]', function(e) {
      return llena_municipio($(this), root, sincoord);
    });
    $(document).on('change', 'select[id$=_departamento_id]', function(e) {
      return llena_municipio($(this), root, sincoord);
    });
    $(document).on('change', 'select[id$=_municipio_id]', function(e) {
      return llena_clase($(this), root, sincoord);
    });
    $(document).on('change', 'select[id$=_municipio_id]', function(e) {
      return llena_clase($(this), root, sincoord);
    });
    $(document).on('change', 'select[id$=_clase_id]', function(e) {
      return pone_tipourbano($(this));
    });
    $(document).on('change', 'select[id$=_clase_id]', function(e) {
      return pone_tipourbano($(this));
    });
    $('#mundep').on('focusin', function(e) {
      msip_arregla_puntomontaje(root);
      return busca_gen($(this), null, root.puntomontaje + "mundep.json");
    });
    $(document).on('click', 'a.enviarautomatico[href^="#"]', function(e) {
      msip_enviarautomatico_formulario($('form'), 'POST', 'json', false);
    });
    $(document).on('change', 'select[data-enviarautomatico]', function(e) {
      return msip_enviarautomatico_formulario($(e.target.form));
    });
    $(document).on('change', 'input[data-enviarautomatico]', function(e) {
      return msip_enviarautomatico_formulario($(e.target.form));
    });
    iniciaRotador = function() {
      $("html").css("cursor", "progress");
    };
    detieneRotador = function() {
      $("html").css("cursor", "auto");
    };
    $(document).on('turbo:click', function(event) {
      if (event.target.getAttribute('href').charAt(0) === '#') {
        return event.preventDefault();
      }
    });
    $(document).on("page:fetch", iniciaRotador);
    $(document).on("page:receive", detieneRotador);
  };

  this.msip_ejecutarAlCargarPagina = function(root) {
    var evento;
    console.log('Msip: Ejecutando al cargar pagina');
    if (typeof window.formato_fecha === 'undefined' || window.formato_fecha === '{}') {
      msip_inicializaMotor();
    }
    $('[data-behaviour~=datepicker]').datepicker({
      format: root.formato_fecha,
      autoclose: true,
      todayHighlight: true,
      language: 'es'
    });
    $('[data-toggle="tooltip"]').tooltip();
    $(document).on('cocoon:after-insert', function(e) {
      $('[data-behaviour~=datepicker]').datepicker({
        format: root.formato_fecha,
        autoclose: true,
        todayHighlight: true,
        language: 'es'
      });
      return $('[data-toggle="tooltip"]').tooltip();
    });
    $('.chosen-select').chosen({
      allow_single_deselect: true,
      no_results_text: 'No hay resultados',
      placeholder_text_single: 'Seleccione una opción',
      placeholder_text_multiple: 'Seleccione algunas opciones',
      width: '100%'
    });
    $('select[data-toggle="tooltip"]').each(function(v, e) {
      var ej, id, ns, t;
      ej = $(e);
      t = ej.attr('data-original-title');
      id = ej.attr('id');
      ns = '#' + id + '_chosen .chosen-choices';
      $(ns).attr('title', t);
      return $(ns).attr('data-toggle', 'tooltip');
    });
    MsipIniciar();
    msip_ajax_recibe_json(root, 'temausuario', {}, msip_pone_tema);
    jQuery(function() {
      $("a[rel~=popover], .has-popover").popover();
      return $("a[rel~=tooltip], .has-tooltip").tooltip();
    });
    evento = new Event('msip:cargado');
    return document.dispatchEvent(evento);
  };

  this.msip_edadDeFechaNacFechaRef = function(anionac, mesnac, dianac, anioref, mesref, diaref) {
    var na;
    if (typeof anionac === 'undefined' || anionac === '') {
      return -1;
    }
    if (typeof anioref === 'undefined' || anioref === '') {
      return -1;
    }
    na = anioref - anionac;
    if (typeof mesnac !== 'undefined' && mesnac !== '' && mesnac > 0 && typeof mesref !== 'undefined' && mesref !== '' && mesref > 0 && mesnac >= mesref) {
      if (mesnac > mesref || (dianac !== 'undefined' && dianac !== '' && dianac > 0 && diaref !== 'undefined' && diaref !== '' && diaref > 0 && dianac > diaref)) {
        na--;
      }
    }
    return na;
  };

}).call(this);




