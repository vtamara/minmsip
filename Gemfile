source "https://rubygems.org"

git_source(:gitlab) { |repo| "https://gitlab.com/#{repo}.git" }

ruby "3.2.2"

gem "babel-transpiler"           # Permite tener módulos ES6

gem "bcrypt", "~> 3.1.7"

gem "bootsnap", require: false

gem "cancancan"                  # Control de acceso

gem "cocoon", git: "https://github.com/vtamara/cocoon.git",
  branch: "new_id_with_ajax"       # Formularios anidados por reemplazar con turbo

gem "coffee-rails"               # Parte de msip aún usa Coffescript

gem "devise"                     # Autenticación

gem "devise-i18n"                # Localización e Internacionalización

gem "dotenv-rails"               # Configuración de servidor en .env

gem "jbuilder"

gem "jsbundling-rails"

gem "kt-paperclip"               # Anexos

gem "nokogiri"                   # Procesamiento XML

gem "pg", "~> 1.1"

gem "puma", ">= 5.0"

gem "rails", "~> 7.1.2"

gem "rails-i18n"                 # Localización e Internacionalización

gem "sassc-rails"                # Conversión a CSS

gem "simple_form"                # Formularios

gem "sprockets-rails"

gem "stimulus-rails"

gem "turbo-rails"

gem "twitter_cldr"               # Localización e internacionalización

gem "will_paginate"              # Pagina listados


#### Motores que sobrecargan vistas o basados en MSIP en orden de apilamento

gem "msip",                       # SI estilo Pasos de Jesús
  git: "https://gitlab.com/pasosdeJesus/msip.git"


group :development, :test do
  gem "code-scanning-rubocop"   # Busca fallas de seguridad

  gem "colorize"

  gem "debug", platforms: %i[ mri windows ]

  gem "rails-erd"               # Genera diagrama entidad asociación
end

group :development do
  gem "web-console"
end

group :test do
  gem "minitest"

  gem "rails-controller-testing"

  gem "simplecov"
end
